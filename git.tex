\documentclass[aspectratio=169]{beamer}

%\setbeameroption{show notes on second screen}

% Remove navigation symbols
\beamertemplatenavigationsymbolsempty%

% Add slide numbers
\setbeamertemplate{footline}{
	\quad%
	\usebeamercolor[fg]{page number in head/foot}%
	\usebeamerfont{page number in head/foot}%
	\insertframenumber%
	\kern1em\vskip2pt%
}
% Cloogle colorscheme
\usepackage{beamercolorthemecloogle}

%Big notes
\AtBeginNote{\huge}

%Set the image directory
\graphicspath{{img/}}

% British locale
\usepackage[english]{babel}

% IPA
\usepackage{tipa}

% Diagrams
\usepackage{tikz}
\usetikzlibrary{positioning,shapes,fit}

% Listings
\usepackage{listings}
\lstset{%
	language=bash,
	basewidth=0.43em,
	basicstyle=\tt\footnotesize,
	breakatwhitespace=false,
	breaklines=true,
	commentstyle=\sl,
	keepspaces=true,
	keywordstyle=\bfseries,
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	stringstyle=\it,
	tabsize=2,
	aboveskip=0pt,
	belowskip=0pt,
	escapeinside={[+}{+]}, % chktex 9
}

\title{git}
\subtitle{a pragmatic workshop}
\titlegraphic{\includegraphics[width=.3\linewidth]{logo.jpg}}

\author{New Devices Lab}
\date{2022}

\begin{document}
\begin{frame}
	\maketitle
\end{frame}

\section{What is git}
\begin{frame}{What is git}
	\begin{figure}[!h]
		\centering
		\includegraphics[width=.5\textwidth]{gitlogo}
	\end{figure}
	\pause%
	\begin{block}{Definition}
		\large Git (/\textipa{[g\i{}t]}/) is a distributed version-control system for tracking changes\only<1>{ in source code during software development}. \qquad --- Wikipedia
	\end{block}
	\note[item]{Developed by Linus Torvalds}
	\note[item]{Used for the linux kernel}
	\note[item]{git $\neq$ github $\neq$ gitlab}
\end{frame}

\begin{frame}[fragile,shrink]{Git gud}
	\begin{block}{Linux}
		Install from your favourite package manager:
		
		\begin{lstlisting}
$ apt-get install git
$ pacman -S git
...\end{lstlisting}
		
		or download and install from \url{https://git-scm.com/download/linux}
	\end{block}
	\pause%
	\begin{block}{Mac OS}
		Download and install from \url{https://git-scm.com/download/mac}
	\end{block}
	\note<3->[item]{Your can use git from homebrew as well}
	\pause%
	\begin{block}{Windows}
		Download and install from \url{https://git-scm.com/download/win}
	\end{block}
	\pause%
	\begin{block}{GUI}
		\pause%
		Not recommended
		\note<4->[item]{GUI's often show less information and make some things more complicated.}
	\end{block}
\end{frame}

\begin{frame}{Git for the New Devices Lab}
	\begin{block}{Git providers}
		\begin{itemize}
			\item \url{https://github.com}
			\item \url{https://gitlab.com}
			\item \url{https://bitbucket.org}
			\pause\item \url{https://gitlab.science.ru.nl}

				\pause%
				\begin{itemize}
					\item Log in with your science account
					\item One repo per group
					\item \url{https://gitlab.science.ru.nl/ndl1/22-23-project}
						\note<3->{
							\begin{itemize}
								\item gitlab account is made when you have logged in for the first time so do this ASAP so I can find your accound
								\item project creation and permissions are done automatically
								\item ndl1 because ndl was taken in the past and is unavailable for some reason
							\end{itemize}
						}
				\end{itemize}
		\end{itemize}
	\end{block}
	\pause{}
	\begin{block}{Gitlab features}
		\begin{itemize}
			\item Git
			\item Issue tracker
			\item Merge requests
			\item Wiki, CI/CD, Mattermost
		\end{itemize}
	\end{block}
\end{frame}

\section{Using git}
\begin{frame}{What is a git repository}
	\begin{itemize}[<+->]
		\item Blocks of hashed data with references to their parents.
			\note<1->[item]{Block tree is stored as a merkle tree}
		\item Represented in a \path{.git} directory in the root.
			\note<2->[item]{\path{.git} name is customizable}
		\item Interaction happens via the \lstinline{git} command.
		\item Each block of data identified by a hash.
			\note<4->[item]{Special names for hashes are branches, tags, etc.}
		\item Works with textual line based diffs.
		\item Therefore not suitable for binaries.
		\item Be careful with mixing this with things like dropbox.
			\note<7->[item]{I've seen people mess up their repos with dropbox}
	\end{itemize}
\end{frame}

\begin{frame}{General overview \& Terminology}{Warning: intimidating}
	\note[item]{repo is the \path{.git} folder}
	\note[item]{index is in \path{.git} and the actual folder}
	\note[item]{tree is the actual folder}
	\note<3->[item]{stash is something only you have, to quickly stash things}
	\note[item]{remote can be anything, another directory, https, ssh, rsync, etc.}
	\begin{figure}[!h]
		\centering
		\begin{tikzpicture}[arr/.style={line width=.5mm,->}]
			\node (repo) [draw, cylinder, minimum height=3em, minimum width=3em, shape border rotate=90, aspect=.3] {repo\note[item]{repo: the \path{.git} folder}};
			\node (inv) [right=of repo] {};
			\node (tree) [draw, regular polygon, regular polygon sides=4, right=of inv] {tree};
			\node (index) [draw, inner sep=-.15em, node distance=4em,regular polygon, regular polygon sides=3, below=of inv] {Index};
			\node (remote) [draw, inner sep=-.1em, cloud, node distance=5em, left=of repo] {Remote};

			\pause%

			\draw [arr] (repo) -- (tree) node [midway,above] {merge};
			\draw [arr] (tree) to [out=-90,in=0] node [midway,right] {add} (index);
			\draw [arr] (index) -- (tree) node [midway,sloped,above] {checkout};
			\draw [arr] (index) -- (repo) node [midway,sloped,above] {commit};

			\draw [arr] (remote) to [out=10,in=170] node [midway,above] {fetch} (repo);
			\draw [arr] (repo) to [out=-170,in=-10] node [midway,below] {push} (remote);

			\draw [arr] (remote) to [out=30,in=150] node [midway,above] {pull} (tree);

			\pause%

			\node (stash) [draw, dashed, inner sep=-.15em, regular polygon, regular polygon sides=3, node distance=5em, right=of tree] {stash};
			\draw [arr] (tree) to [out=10,in=170] node [midway,above] {stash} (stash);
			\draw [arr] (stash) to [out=-170,in=-10] node [midway,below] {stash apply} (tree);

			\pause%

			\node (local) [draw, fit=(repo) (tree) (index)] {};
			\draw [arr] (remote) to [out=-40,in=180] node [midway,below] {clone} (local);
		\end{tikzpicture}
	\end{figure}
\end{frame}

\section{Git scenarios}
\begin{frame}[fragile]{Create a repository}
	\begin{block}{Create a new repository}
		\begin{lstlisting}
$ git init ndl-project\end{lstlisting}
		\begin{onlyenv}<2|handout:0>
			\begin{lstlisting}
Initialized empty Git repository in /home/frobnicator/ndl-project/.git/
$ cd ndl-project
$ git status
On branch main[+\footnote{may be called master for repositories initialised by gitlab/github}+]

No commits yet

nothing to commit (create/copy files and use "git add" to track)
$ ls -a
.       ..      .git\end{lstlisting}
		\end{onlyenv}
		\pause%
		\note<2->[item]{Origin can also be an ssh connection, setup with keys, ask for help if you want this}
		\begin{lstlisting}
$ git remote add origin https://gitlab.science.ru.nl/ndl1/22-23-project/a-42\end{lstlisting}
	\end{block}
	\pause%
	\begin{block}{Work on an existing repository}
		\begin{lstlisting}
$ git clone https://gitlab.science.ru.nl/ndl1/22-23-project/a-42\end{lstlisting}
		\begin{onlyenv}<3,4|handout:0>
			\begin{lstlisting}
Cloning into 'git'...\end{lstlisting}
			\begin{onlyenv}<3|handout:0>
				\begin{lstlisting}
remote: Enumerating objects: 24, done.
remote: Counting objects: 100% (24/24), done.
remote: Compressing objects: 100% (21/21), done.
remote: Total 24 (delta 7), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (24/24), 42.11 KiB | 6.02 MiB/s, done.
Resolving deltas: 100% (7/7), done.
				\end{lstlisting}
			\end{onlyenv}
			\pause%
			\begin{lstlisting}
$ cd ndl-project
$ git status
On branch main[+\footnote{main may be called master for repositories initialised on a local machine}+]
Your branch is up to date with 'origin/main'.

nothing to commit, working tree clean
$ ls -a
.       ..      .git    .gitignore      .gitlab-ci.yml  ndl-project.ino README.md
			\end{lstlisting}
		\end{onlyenv}
	\end{block}
\end{frame}

\begin{frame}[fragile]{Pushing your code}{You made some changes and want them to go upstream}
	\begin{block}{Add untracked files}
		\begin{lstlisting}
$ git status\end{lstlisting}
		\begin{onlyenv}<+|handout:0>
			\begin{lstlisting}
On branch main
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        ndl-project.ino\end{lstlisting}
		\end{onlyenv}
		\pause%
		\begin{onlyenv}<+>
			\begin{lstlisting}
$ git diff\end{lstlisting}
		\end{onlyenv}
%		\note[item]{Diff shows the line based difference with the current hash}
		\begin{lstlisting}
$ git add README.md ndl-project.ino\end{lstlisting}
	\end{block}

	\begin{block}{Commit changes}
		\begin{lstlisting}
$ git commit -m "Added some bla to the readme and created an arduino sketch"\end{lstlisting}
		\pause%
		\begin{onlyenv}<+>
			\begin{lstlisting}
[main 3a47690] Added some bla to the readme and created an arduino sketch
 2 files changed, 1 insertion(+)
 create mode 100644 ndl-project.ino\end{lstlisting}
		\end{onlyenv}
	\end{block}
	\pause%
	\begin{block}{Push upstream}
		\begin{lstlisting}
$ git push origin main\end{lstlisting}
		\begin{onlyenv}<+|handout:0>
			\begin{lstlisting}
Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
...
To gitlab.science.ru.nl:ndl1/22-23-project/a-42
   9964b75..d7c27ad  main -> main\end{lstlisting}
		\end{onlyenv}
	\end{block}
\end{frame}

\begin{frame}[fragile]{Pulling the latest code}{A group member pushed, you pull}
	\begin{lstlisting}
$ git status
On branch main
nothing to commit, working tree clean
$ git pull origin main\end{lstlisting}
	\begin{onlyenv}<+>
		\begin{lstlisting}
remote: Enumerating objects: 144, done.
...
From gitlab.science.ru.nl:ndl1/22-23-project/a-42
 * branch                main     -> FETCH_HEAD
   58a8559b8..e0354de90  main     -> origin/main
Updating 4a73a9c98..e0354de90
Fast-forward
 README.md                                        |   1 +
 1 files changed, 1 insertions(+), 0 deletions(-)
 create mode 100644 ndl-project.ino
		\end{lstlisting}
	\end{onlyenv}
\end{frame}

\begin{frame}[fragile]{\alert{Merge conflict!}}
	\note[item]{Everyone's nightmare, remove repo and reclone?}
	\begin{columns}[t]
		\begin{column}{.5\textwidth}
			\begin{block}{Pieter}
				\begin{lstlisting}
$ git diff\end{lstlisting}
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
diff --git a/shopping.txt b/shopping.txt
index 78e499b..bce1990 100644
--- a/shopping.txt
+++ b/shopping.txt
@@ -1,3 +1,3 @@
 Apples
-Bananas
+Bananas 7x
 Pineapple\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{lstlisting}
$ git add shopping.txt
$ git commit -m "change bananas to 7"
$ git push origin main
				\end{lstlisting}
			\end{block}
		\end{column}
		\pause%
		\begin{column}{.5\textwidth}
			\begin{block}{Mart}
				\begin{lstlisting}
$ git diff\end{lstlisting}
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
diff --git a/shopping.txt b/shopping.txt
index 78e499b..bce1990 100644
--- a/shopping.txt
+++ b/shopping.txt
@@ -1,3 +1,3 @@
 Apples
-Bananas
+Bananas 5x
 Pineapple\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{lstlisting}
$ git add shopping.txt
$ git commit -m "change bananas to 5"
$ git push origin main\end{lstlisting}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
To https://...
 ! [rejected]        main -> main (fetch first)
error: failed to push some refs to 'https://...'
...\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{lstlisting}
$ git pull origin main\end{lstlisting}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
CONFLICT (content): Merge conflict in shopping.txt
Automatic merge failed; fix conflicts and then commit the result.\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
$ cat shopping.txt
Apples
<<<<<<< HEAD
Bananas 5x
=======
Bananas 7x
>>>>>>> bb2d92a8f39e1b62aff1d4d9c247c56fcb472eeb
Pineapple\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{lstlisting}
$ vi shopping.txt && git commit -a
$ git push origin main\end{lstlisting}
			\end{block}
		\end{column}
	\end{columns}
	\pause%
	\begin{block}{tips}
		\begin{itemize}
			\item Search for \lstinline{<<<} in the files and solve by hand.
			\item Use a diff program to solve it interactively (e.g.\ \lstinline{vimdiff}, \lstinline{atom}, \lstinline{meld}, etc.).
			\item Merge strategy e.g.\ \lstinline{git pull -Xours origin/main}\footnote{\url{https://git-scm.com/docs/merge-strategies}}.
			\item Ask for help
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}[fragile]{Going back in time and divergence}
	\begin{columns}[t]
		\begin{column}{.5\textwidth}
			\begin{block}{Go back to an earlier version}
				\begin{lstlisting}
$ git log --oneline\end{lstlisting}
				\note[item]{without \lstinline{--oneline} the log is very verbose}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
e8ed6ae (HEAD -> main, origin/main) remove apples
868e696 add potatoes
5474fc5 Merge remote-tracking branch 'origin/main'
f39b8ec change bananas to 5
5c4cd3e change bananas to 7
53556e1 initial shopping list\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{lstlisting}
$ git log --oneline --graph --all\end{lstlisting}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
* e8ed6ae (HEAD -> main, origin/main) remove apples
* 868e696 add potatoes
*   5474fc5 Merge remote-tracking branch 'origin/main'
|\
| * 5c4cd3e change bananas to 7
* | f39b8ec change bananas to 5
|/
* 53556e1 initial shopping list\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{lstlisting}
$ git checkout 868e696\end{lstlisting}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
$ cat shopping.txt
Apples
Bananas
Pineapple
Potatoes\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{lstlisting}
$ git checkout main\end{lstlisting}
			\end{block}
			\pause%
			\begin{block}{Revert a change}
				\begin{lstlisting}
$ git revert 868e696\end{lstlisting}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
$ cat shopping.txt
Bananas 5x
Pineapple\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
$ git log --oneline --graph --all
* b6735e5 (HEAD -> main) Revert "add potatoes"
* e8ed6ae (origin/main) remove apples
* 868e696 add potatoes
*   5474fc5 Merge remote-tracking branch 'origin/main'
...\end{lstlisting}
				\end{onlyenv}
			\end{block}
		\end{column}
		\pause%
		\begin{column}{.5\textwidth}
			\begin{block}{Sidetrack (branches)}
				You want to do something and not bother your group with it (for a while).

				\begin{lstlisting}
$ git checkout -b develop
...\end{lstlisting}
				\pause%
				\begin{lstlisting}
$ git push origin develop\end{lstlisting}
				\pause%
				\begin{onlyenv}<+|handout:0>
					\begin{lstlisting}
remote: To create a merge request for develop, visit:
remote:   https://gitlab.science.ru.nl/ndl1/22-23-project/a-42/-/merge_requests/new?merge_request%5Bsource_branch%5D=develop
remote:\end{lstlisting}
				\end{onlyenv}
				\pause%
				\begin{itemize}
					\item Assign MR to someone
					\item Allow code review
					\item Merge when CI succeeds
					\item Attach to issues, milestones and labels
				\end{itemize}
			\end{block}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]{\path{.gitignore}}
	\note[item]{Binaries are unsuitable for the diffing algorithm}
	\begin{itemize}
		\item File instructing git what to ignore
		\item Ignore everything that is generated
		\item Glob patterns
	\end{itemize}
	\pause%
	\begin{columns}[t]
		\begin{column}{.20\textwidth}
			\begin{block}{\LaTeX}
				\begin{lstlisting}
*.pdf
*.log
*.aux
*.toc
				\end{lstlisting}
			\end{block}
		\end{column}
		\pause%
		\begin{column}{.20\textwidth}
			\begin{block}{C/C++}
				\begin{lstlisting}
a.out
a.exe
*.tab.[ch]
*.yy.[ch]
*.o
*.a
*.so
				\end{lstlisting}
			\end{block}
		\end{column}
		\pause%
		\begin{column}{.20\textwidth}
			\begin{block}{Clean}
				\begin{lstlisting}
Clean System Files
*-www
*-data
*.pbc
*.bc
nitrile-packages
				\end{lstlisting}
			\end{block}
		\end{column}
		\pause%
		\begin{column}{.20\textwidth}
			\begin{block}{arduino-mk}
				\begin{lstlisting}
.build
				\end{lstlisting}
			\end{block}
		\end{column}
		\pause%
		\begin{column}{.20\textwidth}
			\begin{block}{Python}
				\begin{lstlisting}
__pycache__/
*.so
# virtualenv stuff
build/
dist/
lib/
lib64/
var/
				\end{lstlisting}
			\end{block}
		\end{column}
	\end{columns}
\end{frame}

\section{Conclusion}
\begin{frame}{Conclusion}
	\begin{itemize}[<+->]
		\item We have created repos for you and grant you access.
			\note[item]{So log in to your science account!}
		\item Setup your \path{.gitignore} carefully.
			\note<2->[item]{setup the ignore to avoid accidental commits of object files, executables, etc.}
		\item Never use force flags (\lstinline{-f}) flags.
			\note<3->[item]{force flags can make you lose code}
		\item Ask Mart\footnote{\url{mart@cs.ru.nl}} for guidance with complex matters.
			\note<4->[item]{complex matters: ssh keys, CI/CD, migration, conflicts}
		\item If you want to understand it better:

			Git from the Bottom Up --- John Wiegley:

			\url{https://jwiegley.github.io/git-from-the-bottom-up/}
			\note<5->[item]{\Large Git from the bottom up is a truly recommended read, only $\pm$30pg}

		\item These slides are available here:

			\url{https://gitlab.science.ru.nl/ndl1/git}
	\end{itemize}
\end{frame}

\end{document}
